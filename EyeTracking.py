from Graphics import *
from datetime import datetime, timedelta
import random
import math
import time


# CONFIGURATION OPTIONS:
#size of the window in pixels (x,y)
res = [1024, 768]
#velocity of target in pixels per second for moving targets
velocity = 150.0
#refresh rate to use
fps = 60.0
#background color to use
backgroundColor = "grey"
#target color to use
targetColor = "red"
#color of the text displayed on the screen
textColor = "red"
#size of the circle to draw
targetSize = 10
#whether or not the target moves at a uniform speed
uniformSpeed = True
#A multiplier that controls the maximum vertical delta. Lower numbers indicate a smaller max change. range 0.0 to 1.0
deltaYMultiplier = 1.0
# END OF CONFIGURATION

mode = 0


#generates 2 floats between -1 and 1 that serve as directional headers. They are linked geometrically such that the
#target will always be moving at the same speed
def uniformDirection():
	xSpeed = random.uniform(-1, 1)
	ySpeed = math.sqrt(1 - (xSpeed * xSpeed)) * random.choice([-1, 1])
	return [xSpeed, ySpeed]


#generates 2 floats between -1 and 1 that serve as directional headers. They will not always give the max speed
def randomDirection():
	xSpeed = random.uniform(-1, 1)
	ySpeed = random.uniform(-1, 1)
	return [xSpeed, ySpeed]


def newDirection():
	if uniformSpeed:
		return uniformDirection()
	else:
		return randomDirection()


def main():
	win = GraphWin("Eye Tracking", res[0], res[1])
	win.setBackground(backgroundColor)
	c = Circle(Point(res[0]/2, res[1]/2), targetSize)
	c.setFill(targetColor)
	c.setOutline(targetColor)
	c.draw(win)
	print("press q to quit")
	# Unmoving circle in center
	if mode == 0:
		win.getKey() # pause for key input
		win.close()
	# Moving circle, but no interaction
	elif mode == 1:
		direction = uniformDirection()
		while not win.checkKey() == "q":
			if c.getCenter().getX() < 0:
				c.move(5, 0)
				direction = newDirection()
			elif c.getCenter().getX() > res[0]:
				c.move(-5, 0)
				direction = newDirection()
			if  c.getCenter().getY() < 0:
				c.move(0, 5)
				direction = newDirection()
			elif c.getCenter().getY() > res[1]:
				c.move(0, -5)
				direction = newDirection()
			c.move(direction[0] * velocity / fps, direction[1] * velocity / fps)
			time.sleep(1.0 / fps)
	# 5 circle test eye pattern
	elif mode == 2:
		c1 = Circle(Point(res[0]/2 + 150, res[1]/2), targetSize)
		c2 = Circle(Point(res[0]/2 - 150, res[1]/2), targetSize)
		c3 = Circle(Point(res[0]/2, res[1]/2 + 150), targetSize)
		c4 = Circle(Point(res[0]/2, res[1]/2 - 150), targetSize)
		c1.setFill(targetColor)
		c1.setOutline(targetColor)
		c1.draw(win)
		c2.setFill(targetColor)
		c2.setOutline(targetColor)
		c2.draw(win)
		c3.setFill(targetColor)
		c3.setOutline(targetColor)
		c3.draw(win)
		c4.setFill(targetColor)
		c4.setOutline(targetColor)
		c4.draw(win)
		win.getKey() # pause for mouse input
		win.close()
	# Target that is tracked without clicking
	elif mode == 3:
		hitCount = 0
		score = Text(Point(res[0] / 2, 25), "Score: 0")
		timeText = Text(Point(res[0] / 2, 40), "0.0")
		start = datetime.now().timestamp()
		end = datetime.now().timestamp()
		score.setFill(textColor)
		timeText.setFill(textColor)
		score.draw(win)
		timeText.draw(win)
		while not win.checkKey() == "q":
			mouse = win.checkMouse()
			timeText.setText(str(math.floor((datetime.now().timestamp() - start) * 10) / 10.0))
			if mouse != None:
				circle = c.getCenter()
				diffX = mouse.getX() - circle.getX()
				diffY = mouse.getY() - circle.getY()
				diff = math.sqrt(diffX * diffX + diffY * diffY)
				if diff < targetSize:
					hitCount += 1
					score.setText("Score: " + str(hitCount))
					dx = random.randint(targetSize, res[0] - targetSize) - circle.getX()
					dy = random.randint(targetSize, res[1] - targetSize) - circle.getY() * deltaYMultiplier
					end = datetime.now().timestamp()
					c.move(dx, dy)
		print("Elapsed time: " + str(end - start) + " sec")

#preconfigure from args
if len(sys.argv) > 1 and sys.argv[1] == "-m":
	mode = 1
elif len(sys.argv) > 1 and sys.argv[1] == "-p":
	mode = 2
elif len(sys.argv) > 1 and sys.argv[1] == "-t":
    mode = 3
main()
