Eye Tracking
==

Last Updated: 26-7-2018
Current Version: Beta 0.2
## Description
---
This project provides some simple eye and brain training exercises, meant specifically to increase coordination in those with physical challenges with their eyes and hands. It also provides a simple game that gives a simple aim training simulator, and settings to customize it to the users desires.


## Usage
---
**Installing:**

Install the latest version of python 3 for your device, found [here](https://www.python.org/downloads/).

Clone or download this project to your device, somewhere easily accessible by you.

On some distros of linux, you may have to install `python-tk` in order for this project to work

**Running**

This program is a terminal script, with several options. On unix-like platforms, change your current directory using the command:
```
cd your/directory/EyeTracking/
```
On Windows, use:
```
cd your\directory\EyeTracking\
```
To run the script:
```
python EyeTracking.py [-m] [-p] [-t]
```
All 3 flags are optional, but mutually exclusive.

no flags: A single, unmoving dot appears in the middle of the screen.
![Image](pictures/basic.png)

`-m`: A moving dot appears on the screen, but the user cannot interact with it. For eye training only.
![Image](pictures/motion.png)

`-p`: A 5-dot pattern is shown on the screen. For eye and muscle training only.
![Image](pictures/pattern.png)

`-t`: A Target is shown on the screen, and does not move until the user clicks it. A score is tracked on the top, along with a timer that reports how long you have been going for.
![Image](pictures/target.png)

For both platforms, there is a script that simplifies running target practice (`-t`) mode.

**Modifying the config**

To change the parameters of the script, open `EyeTracking.py` in any text editor. in between `CONFIGURATION OPTIONS:` and `END OF CONFIGURATION` are several variables, and their descriptions. Use these to modify the look and difficulty of the various modes.
